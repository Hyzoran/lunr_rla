# LUNR_RLA - libunr Rogue-like Adventure
## 	Rogue-like game framework for libunr/Unreal.
	
## Summary

LUNR RLA provides a configurable framework for providing a rogue-like coop 
experience in the Unreal/libunr sandbox, with an emphasis on Exploration, Generation, 
Loot, Growth, Variety, and Teamwork.

Currently, this readme is just a design plan.

## Features

### Maps/Overworld
	
	RLA generates an overworld "map", which can arbitrarily link different maps or 
	mappacks together. Each map is represented by a node, and forms links to other 
	nodes based on mappack configurations. Extra links may randomly generated or pre-defined ones 
	either via config or by mapper design. A good example of this is Kirby and the Amazing Mirror.
	
	Any map that does not contain a link to another map will be given one, (preferably via config 
	for intelligent placement), or any map that contains an invalid teleporter link will be treated
	as a link to another map. The overworld will assemble these different maps together to form a 
	larger environment. Optionally, maps may be re-used to provide an infinite overworld, but these
	are treated as different instances.
	
	As libunr will provide support for running multiple maps at once, players may seamlessly travel 
	between nodes freely, bi-directionally.
	
	Random events may take place on the overworld, such as particular nodes having some kind of
	"quirk", or events that propagate from one node to another over time, such as roaming bands
	of enemies.
	
	Admins may even provide map definitions to support non-coop maps, such as DM, CTF, etc. maps.
	Allowing these maps to become part of the overworld and thus the game.
	
	Even when no players are present in a map, its state will be preserved.
	
	RLA may even be used only for the overworld features and nothing else.
		
### Procedural Levels
	
	In addition to pre-made levels, RLA may be configured to generate original levels as a node.
	This generate will have its own configuration system, such as defining assets, textures,
	etc. as part of a theme to be used in this level. A level may have one or more themes.
		
### Content
	
	All content to be used by RLA will be defined by definition units provided by the admin
	to add support for certain classes/assets, with how to use them, if nessecary.
	
	Definition units can be shared publically to allow faster configuration of well-known mods.
	
	RLA may then use these classes as-is or may optionally apply procedural generation elements to
	them, such as modifying their qualities.
		
### Inventory
	
	RLA will implement a Diablo-style inventory system, with the exception that items/abilities/weapons 
	will use a hot-bar system, or be used directly from inventory. As such, this hotbar replaces
	the traditional Unreal Item and Weapon switching functionality. This allows for on-the fly
	hotkeys for switching or using items.
	
	RLA may generate items and apply different modifiers to them using a trait system, using "Real" classes
	as a base. Some inventory items may be used to modify these items and apply/remove/adapt traits to them.
		
### Abilities/Traits
	
	Admins may add ability classes for RLA to use, although RLA provides its own stock set.
	
	Abilities are simply effects that may be added to the hotbar and casted. Passive abilities also exist.
	
	Traits are modifiers that provide passive/active abilities, or any arbitrary effect. They can be 
	applied to monsters, inventory items, maps, etc.
		
### Monsters/Creeps
	
	Monsters included with maps will be preserved, or optionally removed in favour of complete RNG-spawned
	ones. Monsters included in these maps may also be modified with traits/difficulty by the RLA system.
	
	Depending on the activity trait of the node, monsters may randomly spawn over time.
		
### Progression/Stats
	
	RLA uses a growth tree for players to traverse and spend their EXP points on, similar to POE or FFX.
	This tree may be a static definition or randomly generated. However, all players use the same skill tree.
	The skill tree may allow infinite progression with randomly generated nodes if enabled.
	
	Admins may also define exp/level curves, starting exp, etc.
		
### Crafting
	
	RLA will provide a crafting system. Materials may be collected from nodes randomly 
	
### Difficulty
	
	Difficulty may scale in different ways. Admins may configure difficulty to scale by player strength, 
	per node (distance from origin), or by time, giving a sense of urgency to player growth.
		
### Gamemodes
	
	Different gamemodes may be used for the RLA system. Depending on the gamemode, they may have a 
	"staging" session. For example,
	
	Survival (Default) - All players are part of a party. Players must grow their power and stay alive
	as long as possible. Deaths are optionally permanent, and may be live-based or single-life. Revival may be enabled
	or disabled with certain skills or items, by proximity or at any time. Players may optionally join mid-game. 
	Once no more players are alive on the server (by config, either in the database or current present), 
	the world restarts.
	
	Survival, paired with difficulty over time, with older/earlier maps accelerating in difficulty, may provide 
	an intense urgency to explore to grow in power, and move around to avoid threats.
	
	LMS/Royale - Team or FFA, players spawn apart from eachother, unaware of the locations or movements of others.
	Overworlds are non-infinite, and relatively small. The goal is to be the last team or man standing. Mutual 
	enemies (Monsters) may optionally still spawn. This mode only supports temporary persistence.
	
	Hide and Seek/Mutant - Similar to the above, except one player is the mutant, and all the others 
	must find and kill this mutant to become the mutant themselves. Only the mutant may gain score, and 
	may do so by collecting treasure and/or killing players. In team mode, "mutant" status is based on 
	possession of a flag instead.
	
	Perhaps some Territory-based mode? Warfare/Onslaught over multiple maps?
		
### Persistence/Instances
	
	A server running RLA may maintain one or more instances of the overworld.
	
	Admins may configure how RLA terminantes persistence of Player progression/world state,
	whether it be permanent, by "seasons", or by after certain game conditions. This is based
	on the RLA gamemode.